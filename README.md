# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

# Status da aplicação:
Não foi finalizada, a ligação da interface com a parte de controle e modelagem não foi terminada!

# Como executar a interface:
1-Baixe ou clone o repositorio do projeto em seu computador;
2-Abra o projeto atraves da IDE netbeans;
3-Execute a classe EmpregadoLogado;
4-Na aba de "Realizar Pedido" selecione o pedido e clique em adicionar, caso queira remover clique no pedido e em remover;
5-Na aba de "caixa" selecione a opção de pagamento;
6-Na aba de "estoque" consultaria o estoque, porém não foi terminada;