package armazenamento;

import java.util.ArrayList;
import java.util.List;
import model.Pedido;

public class Dados {
    public static List<Pedido> pedidos = new ArrayList<>();
    
    public static final int minStock = 4;
    public static final int maxStock = 20;
    
    public static void addPedido(Pedido pedido){
        pedidos.add(pedido);
    }
}
