package model;

public class PagamentoEmDinheiro extends Pagamento{
    private int valorPago;

    public int getValorPago() {
        return valorPago;
    }

    public void setValorPago(int valorPago) {
        this.valorPago = valorPago;
    }
    
}
