package model;

public class Produto {
    
    private String nome;
    private double valor;
    private int numProduto;
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(){
        this.nome = nome;
    }
    
    public double getValor(){
        return valor;
    }
    
    public void setValor(double valor){
        this.valor = valor;
    }
    
    public int getNumProduto(){
        return numProduto;
    }
    
    public void setNumProduto(){
        this.numProduto = numProduto;
    }
    
}
